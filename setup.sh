#!/usr/bin/env bash

# exit on any error
set -e

# install dependencies
apt update
apt install -y libtool libusb-1.0-0-dev librtlsdr-dev rtl-sdr build-essential autoconf cmake pkg-config python-influxdb

# download, build and install rtl_433
curl -L https://github.com/merbanan/rtl_433/archive/20.02.tar.gz | tar xvz
cd rtl_433-20.02/
mkdir build
cd build
cmake ..
make
make install

# install oil-tank-monitor script, env and service files
curl -L https://gitlab.com/mccleanp/oil-tank-monitor/-/raw/master/oil-tank-monitor.py -o /usr/local/bin/oil-tank-monitor.py
chmod +x /usr/local/bin/oil-tank-monitor.py
curl -L https://gitlab.com/mccleanp/oil-tank-monitor/-/raw/master/oil-tank-monitor.env -o /usr/local/etc/oil-tank-monitor.env
curl -L https://gitlab.com/mccleanp/oil-tank-monitor/-/raw/master/oil-tank-monitor.service -o /etc/systemd/system/oil-tank-monitor.service

# configure
editor /usr/local/etc/oil-tank-monitor.env </dev/tty

# start
systemctl enable oil-tank-monitor
systemctl start oil-tank-monitor

echo "Oil Tank Monitor installed successfully"
