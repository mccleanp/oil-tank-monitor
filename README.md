# Oil Tank Monitor

Simple oil tank monitor to measure oil tank depth and log to influxdb.

Depth is measured using an off-the-shelf Watchman Sonic Oil Level monitor. This broadcasts on 433Mhz and is normally received by the supplied receiver plug. However using a Raspberry PI with a Software Defined Radio (SDR) module, these broadcasts can be received and logged to an influxdb database. The [python script](oil-tank-monitor.p) receives the broadcast depth, computes the oil volume, and then logs it to the influxdb database.

The oil volume is calculated using details of the tank height and maximum volume, assuming the tank to be uniform. The Watchman also broadcasts temperature, which is also logged to influxdb. No attempt is made to correct the volume measurements for the effects of temperature as the discrepancy is likely less than the error in the measured value.

This project makes use of the excellent [rtl_433](https://github.com/merbanan/rtl_433) program to decode the 433Mhz radio signal.

## Required Hardware

* [Watchman Sonic Oil Level Monitor](https://www.amazon.co.uk/Watchman-Sonic-Oil-Level-Monitor/dp/B0032Q8Q64)
* [Software Defined Radio Receiver USB Stick (RTL2832)](https://www.amazon.co.uk/gp/product/B00HXF8WLY)
* [Raspberry PI](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)


## Software Installation

Install [Raspbian Buster Lite](https://www.raspberrypi.org/downloads/raspbian/), then on the Raspberry PI console, run the following command to install the oil-tank-monitor.py script and necessary dependencies.

```bash
curl -L https://gitlab.com/mccleanp/oil-tank-monitor/-/raw/master/setup.sh | sudo bash
```

Review the [setup script](setup.sh) script for more information. If you are in a sensitive security environment, it is sensible to download, audit and execute the script manually, rather than relying on the above one-liner.

During the setup you will need to provide your tank height and volume and connection details for your influxdb server. The setup script will install a service, enable it to start automatically and start it immediately.

Note that ordinarily the Watchman only broadcasts depth readings approx once per hour. However if the receiver plug is briefly held against the transmitter sensor, matching up the spots, it will broadcast readings every second for several minutes, before returing to the normal broadcast frequency.

