#!/usr/bin/env python

import subprocess
import os
import json
from influxdb import InfluxDBClient

TANK_HEIGHT=int(os.environ['TANK_HEIGHT'])
TANK_VOLUME=int(os.environ['TANK_VOLUME'])

client = InfluxDBClient(
        os.environ['INFLUXDB_HOST'],
        os.environ.get('INFLUXDB_PORT'),
        os.environ.get('INFLUXDB_USER'),
        os.environ.get('INFLUXDB_PASS'),
        os.environ['INFLUXDB_DATABASE']);

process = subprocess.Popen(['rtl_433', '-R', '43', '-F', 'json', '-M', 'time:iso'], stdout=subprocess.PIPE)
while True:
  output = process.stdout.readline()
  if output == '' and process.poll() is not None:
    break
  try:
    data = json.loads(output)
    level = TANK_HEIGHT - data['depth_cm']
    fill = float(level) / TANK_HEIGHT
    volume = fill * TANK_VOLUME
    points = [ {
        'measurement': 'oil-tank',
        'time': data['time'],
        'fields': {
          'temperature': data['temperature_C'],
          'depth': data['depth_cm'],
          'level': level,
          'fill': fill,
          'volume': volume
        }
      } ]
    client.write_points(points)
  except:
    pass
